import { Injectable } from '@angular/core';

// With injectable root we don't need to declare MessagesService inside providers from app.module
// @Injectable({
//     providedIn: 'root',
// })

// If made straight with Injectable without provideIn, make sure to put MessagesService in app.module providers
@Injectable()
export class MessagesService {
    private messageList: string[];
    constructor() {
        this.messageList = ['Hello!', "I'm John", 'How are you?'];
    }
    getMessageList(): string[] {
        return this.messageList;
    }
    pushNewMessage(message: string): void {
        this.messageList.push(message);
    }
}
