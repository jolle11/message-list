import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessageListComponent } from './pages/message-list/message-list.component';
import { NewMessageComponent } from './pages/new-message/new-message.component';
import { MessagesService } from './shared/services/messages.service';

@NgModule({
    declarations: [AppComponent, MessageListComponent, NewMessageComponent],
    imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
    providers: [MessagesService],
    bootstrap: [AppComponent],
})
export class AppModule {}
