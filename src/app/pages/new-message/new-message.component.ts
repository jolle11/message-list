import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/shared/services/messages.service';

@Component({
    selector: 'app-new-message',
    templateUrl: './new-message.component.html',
    styleUrls: ['./new-message.component.scss'],
})
export class NewMessageComponent implements OnInit {
    messageForm: FormGroup = new FormGroup({});
    constructor(private messagesService: MessagesService) {}

    ngOnInit(): void {
        this.messageForm = new FormGroup({
            message: new FormControl('', Validators.minLength(2)),
        });
    }
    onSubmit(): void {
        this.messagesService.pushNewMessage(this.messageForm.value.message);
        this.messageForm.reset();
    }
}
