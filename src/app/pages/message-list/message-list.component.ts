import { Component, OnInit } from '@angular/core';
import { MessagesService } from 'src/app/shared/services/messages.service';

@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message-list.component.scss'],
})
export class MessageListComponent implements OnInit {
    list: string[] = [];

    constructor(public messagesService: MessagesService) {}

    ngOnInit(): void {
        this.list = this.messagesService.getMessageList();
    }
}
